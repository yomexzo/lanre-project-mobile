package com.pusher.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class TestsActivity extends ActionBarActivity {
    ListView listView;
    View progressView;
    TestAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tests);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        listView = (ListView) findViewById(R.id.testList);
        progressView = findViewById(R.id.testProgress);
        System.out.println(progressView);

        adapter = new TestAdapter(getRunningInstance(), getResources());
        listView.setAdapter(adapter);

        populateTests();
    }


    @Override
    public Intent getSupportParentActivityIntent (){

        Intent parentIntent= getIntent();
        String className = parentIntent.getStringExtra("ParentClassName"); //getting the parent class name

        Intent newIntent=null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(TestsActivity.this,Class.forName("com.pusher.testapp."+className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tests, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_faq) {
            Intent intent = new Intent(this, FAQActivity.class);
            intent.putExtra("ParentClassName","TestsActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    public void showProgress(final boolean show) {
        {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            listView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    public TestsActivity getRunningInstance(){
        return this;
    }
    public void populateTests(){
        showProgress(true);
        ArrayList<NameValuePair> conditions = new ArrayList<NameValuePair>();
        conditions.add(new BasicNameValuePair("patient_id", ""+((App) getApplicationContext()).getPatient().id));
        FetchTest fetch = new FetchTest(conditions);
        fetch.execute();
    }
    public class FetchTest extends AsyncTask<Void, Void, Void> {

        ArrayList<NameValuePair> conditions = new ArrayList<NameValuePair>();
        FetchTest(ArrayList<NameValuePair> conditions) {
            this.conditions = conditions;
        }

        @Override
        protected Void doInBackground(Void... params) {


            String url = getString(R.string.server_address) + getString(R.string.list_tests_end_point);
            String response = Service.postRequest(url, conditions);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.clear();
                }
            });

            if(response != null){
                try {

                    JSONArray response_tests = new JSONArray(response);
                    if(response_tests.length() > 0){
                        for(int i=0; i < response_tests.length(); i++){
                            JSONObject combination = response_tests.getJSONObject(i);
                            JSONObject resp_test = combination.getJSONObject("Test");
                            JSONObject resp_doc_pat_test = combination.getJSONObject("DoctorPatientsTest");
                            final Test test = new Test(
                                    resp_test.getInt("id"),
                                    resp_doc_pat_test.getInt("id"),
                                    resp_test.getString("name"),
                                    resp_test.getString("description"),
                                    resp_test.getString("created"),
                                    resp_test.getString("price"),
                                    resp_test.getString("available"),
                                    combination.getJSONObject("DoctorPatient").getJSONObject("Doctor").getString("name")

                            );
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.add(test);
                                }
                            });
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(final Void v) {
            onCancelled();

        }

        @Override
        protected void onCancelled() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                }
            });
        }
    }

}
class Test{
    public int id, doctor_patient_id;
    public String name, description, created, price, available, doctorName;
    public Test(int id, int doctor_patient_id, String name, String description, String created, String price, String available, String doctorName){
        this.id = id;
        this.doctor_patient_id = doctor_patient_id;
        this.name = name;
        this.description = description;
        this.created = created;
        this.price = price;
        this.available = available;
        this.doctorName = doctorName;
    }
}
class TestAdapter extends BaseAdapter{
    private Activity activity;
    private ArrayList<Test> data = new ArrayList<Test>();
    private static LayoutInflater inflater=null;
    public Resources res;
    Test test=null;
    int i=0;
    public TestAdapter(Activity a, Resources resLocal){
        activity = a;
        res = resLocal;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        return data.size();
        return data.size() <= 0 ? 1 : data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.size() <= 0 ? i : data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public boolean add(Test t){
        boolean added = this.data.add(t);
        if(added)
            this.notifyDataSetChanged();
        return added;
    }
    public void clear(){
        this.data.clear();
        this.notifyDataSetChanged();
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView name;
        public TextView description;
//        public TextView textWide;
//        public ImageView image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate test_item.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.test_item, null);

            /****** View Holder Object to contain test_item.xml file elements ******/

            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.testItemName);
            holder.description = (TextView)vi.findViewById(R.id.testItemDescription);
//            holder.image=(ImageView)vi.findViewById(R.id.image);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.name.setText("No Data");
            holder.description.setText("");
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            test = null;
            test = ( Test ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.name.setText( test.name );
            holder.description.setText(
                    " by Dr "+test.doctorName + " " +
                    test.created
            );
//            holder.image.setImageResource(
//                    res.getIdentifier(
//                            "com.androidexample.customlistview:drawable/"+tempValues.getImage()
//                            ,null,null));

            /******** Set Item Click Listner for LayoutInflater for each row *******/

//            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }
}
