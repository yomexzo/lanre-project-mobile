package com.pusher.testapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


public class RegisterActivity extends ActionBarActivity {

    private EditText name, cardNumber, email, nationality, address, password, passwordConfirmation;
    private Spinner bloodGroup, gender;
    private View progressView;
    private View registerFormView;
    RegisterTask registerTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Button _temp = (Button) findViewById(R.id.go_to_login_button);
        _temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getRunningInstance(), LoginActivity.class);
                startActivity(intent);
            }
        });


        _temp = (Button) findViewById(R.id.register_button);
        _temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        name = (EditText) findViewById(R.id.reg_name);
        cardNumber = (EditText) findViewById(R.id.reg_card_number);
        email = (EditText) findViewById(R.id.reg_email);
        bloodGroup = (Spinner) findViewById(R.id.reg_blood_group);
        gender = (Spinner) findViewById(R.id.reg_gender);
        nationality = (EditText) findViewById(R.id.reg_nationality);
        address = (EditText) findViewById(R.id.reg_address);
        password = (EditText) findViewById(R.id.reg_password);
        passwordConfirmation = (EditText) findViewById(R.id.reg_password_confirmation);


        registerFormView = findViewById(R.id.register_form);
        progressView = findViewById(R.id.register_progress);
    }


    public void register() {
        if (registerTask != null) {
            return;
        }

        // Reset errors.
        name.setError(null);
        cardNumber.setError(null);
        email.setError(null);
        nationality.setError(null);
        address.setError(null);
        password.setError(null);
        passwordConfirmation.setError(null);

        // Store values at the time of the login attempt.
        String _name = name.getText().toString();
        String _cardNumber = cardNumber.getText().toString();
        String _email = email.getText().toString();
        String _bloodGroup = bloodGroup.getSelectedItem().toString();
        String _gender = gender.getSelectedItem().toString();
        String _nationality = nationality.getText().toString();
        String _address = address.getText().toString();
        String _password = password.getText().toString();
        String _passwordConfirmation = passwordConfirmation.getText().toString();


        boolean cancel = false;
        View focusView = null;
        String toastMessage = "";
        if (TextUtils.isEmpty(_name)) {
            name.setError("Can not be empty");
            cancel = true;
        }
        if (TextUtils.isEmpty(_cardNumber)) {
            cardNumber.setError("Can not be empty");
            cancel = true;
        }else if(!Patterns.EMAIL_ADDRESS.matcher(_cardNumber).matches()){
            cardNumber.setError("Email address is not valid ");
            cancel = true;
        }
//        if (TextUtils.isEmpty(_email)) {
//            email.setError("Can not be empty");
//            cancel = true;
//        }else if(!Patterns.EMAIL_ADDRESS.matcher(_email).matches()){
//            email.setError("Email address is not valid ");
//            cancel = true;
//        }
        if (TextUtils.isEmpty(_nationality)) {
            nationality.setError("Can not be empty");
            cancel = true;
        }
        if (_bloodGroup.equalsIgnoreCase("-- CHOOSE BLOOD GROUP --")) {
            toastMessage += "Please select your blood group";
            cancel = true;
        }
        if (_gender.equalsIgnoreCase("-- CHOOSE GENDER --")) {
            toastMessage += (toastMessage.equals("") ? "" : "\n") + "Please select your gender";
            cancel = true;
        }
        if (TextUtils.isEmpty(_address)) {
            address.setError("Can not be empty");
            cancel = true;
        }
        if (TextUtils.isEmpty(_password)) {
            password.setError("Can not be empty");
            cancel = true;
        }
        if (TextUtils.isEmpty(_passwordConfirmation)) {
            passwordConfirmation.setError("Can not be empty");
            cancel = true;
        }
        if (!_password.equals(_passwordConfirmation)) {
            passwordConfirmation.setError("Passwords must be the same");
            cancel = true;
        }
        if(!toastMessage.equalsIgnoreCase("")){
            Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

        if (!cancel) {
            // Show a  progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            ArrayList<NameValuePair> maps = new ArrayList<NameValuePair>();
            maps.add(new BasicNameValuePair("data[Patient][name]", _name));
            maps.add(new BasicNameValuePair("data[Patient][username]", _cardNumber));
            maps.add(new BasicNameValuePair("data[Patient][email_address]", _email));
            maps.add(new BasicNameValuePair("data[Patient][blood_group]", _bloodGroup));
            maps.add(new BasicNameValuePair("data[Patient][gender]", _gender));
            maps.add(new BasicNameValuePair("data[Patient][nationality]", _nationality));
            maps.add(new BasicNameValuePair("data[Patient][address]", _address));
            maps.add(new BasicNameValuePair("data[Patient][password]", _password));

            registerTask = new RegisterTask(maps);
            registerTask.execute((Void) null);
        }
    }

    public void showProgress(final boolean show) {
        {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            registerFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    public class RegisterTask extends AsyncTask<Void, Void, String> {

        ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();

        RegisterTask(ArrayList<NameValuePair> para) {
            parameters = para;
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = getString(R.string.server_address) + getString(R.string.register_end_point);
            String str = Service.postRequest(url, parameters);
            return str;
        }

        @Override
        protected void onPostExecute(final String response) {

            if(response != null){
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getString("status").equalsIgnoreCase("ok")){

                        name.setText("");
                        cardNumber.setText("");
                        email.setText("");
                        nationality.setText("");
                        address.setText("");
                        password.setText("");
                        passwordConfirmation.setText("");

                    }
                    CharSequence text = json.getString("message");
                    Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }else{
                Toast toast = Toast.makeText(getApplicationContext(), "Error encountered communicating to server.", Toast.LENGTH_SHORT);
                toast.show();
            }


            showProgress(false);
            registerTask = null;


        }

        @Override
        protected void onCancelled() {
            registerTask = null;
            showProgress(false);
        }
    }

    private RegisterActivity getRunningInstance(){
        return this;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_faq) {
            Intent intent = new Intent(getRunningInstance(), FAQActivity.class);
            intent.putExtra("ParentClassName","RegisterActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
