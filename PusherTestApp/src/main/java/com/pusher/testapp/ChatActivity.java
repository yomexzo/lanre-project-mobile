package com.pusher.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;


public class ChatActivity extends ActionBarActivity implements PrivateChannelEventListener {

    Pusher pusher;
    PusherOptions options;
    Doctor doctor;
    ListView listView;
    ImageView iv;
    MenuItem refreshMenuItem;
    ArrayList<Chat> chats = new ArrayList<Chat>();
    ChatAdapter adapter;
    PrivateChannel channel;
    String channelName;
    String newMessageEvent = "new_message";
//    private static final String STATE_KEY_CONNECTED = "CONN";
//    private static final String CHANNEL_NAME = "channel";
//    private static final String EVENT_NAME = "event";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            doctor = (Doctor) getIntent().getSerializableExtra("doctor");
        } else {
            doctor= (Doctor) savedInstanceState.getSerializable("doctor");
        }
        if(doctor == null){
            doctor = new Doctor(0, "Go back!");
        }
        setTitle(doctor.name);
        setContentView(R.layout.activity_chat);


        listView = (ListView) findViewById(R.id.list_chat_view);
        setupChannel();
        setupAdapter();
        /* Time to cache inflated ImageView for rotation */
        LayoutInflater inflater = (LayoutInflater) getRunningInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        iv = (ImageView) inflater.inflate(R.layout.refresh_action_view, null);


        Button button = (Button) findViewById(R.id.send_message_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    EditText editText = (EditText) findViewById(R.id.message_edit_text);
                    String txt = editText.getText().toString().trim();
                    if(!txt.equals("")){
                        PushChat push = new PushChat(((App) getApplicationContext()).getPatient().id, doctor.id, URLEncoder.encode(txt, "utf-8"));
                        push.execute((Void) null);
                        editText.setText("");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        populateChat();
    }
    private void setupChannel(){
        App app = (App) getApplicationContext();
        channelName = "private-"+app.getPatient().id+"_"+doctor.id;
        try{
            if(channel == null)
                channel = app.getPusher().subscribePrivate(channelName, this, newMessageEvent);
        }catch(IllegalArgumentException e){
            app.getPusher().unsubscribe(channelName);
            channel = app.getPusher().subscribePrivate(channelName, this, newMessageEvent);
        }
    }
    private void setupAdapter(){
        if(adapter == null){
            System.out.println("new adapter creation");
//            adapter = new ArrayAdapter<Chat>(getRunningInstance(), R.layout.general, R.id.chat_message_text_view, chats);
            adapter = new ChatAdapter(getRunningInstance(), getResources());
            listView.setAdapter(adapter);
        }
    }
    @Override
    protected void onPause(){
        super.onPause();
    }
    @Override
    protected void onResume(){
        setupChannel();
        super.onResume();
    }
    private void updateChatListView(){
//        adapter.notifyDataSetChanged();
        listView.setSelection(adapter.getCount() - 1);
    }
    public void populateChat(){
        int last_chat_id = 0;
        int limit = 40;
        if(chats.size() > 0){
            last_chat_id = (chats.get(chats.size() - 1)).id;
        }
        FetchChat fetch = new FetchChat(((App) getApplicationContext()).getPatient().id, doctor.id, last_chat_id, limit);
        fetch.execute((Void) null);
    }

    public ChatActivity getRunningInstance(){
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat, menu);

        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //put put refresh menu into a variable and hide
        refreshMenuItem = menu.findItem(R.id.action_refresh_chat);
        refreshMenuItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_faq) {
            Intent intent = new Intent(getRunningInstance(), FAQActivity.class);
            intent.putExtra("ParentClassName","ChatActivity");
            startActivity(intent);
        }else if (id == R.id.action_tests) {
            Intent intent = new Intent(getRunningInstance(), TestsActivity.class);
            intent.putExtra("ParentClassName","ChatActivity");
            startActivity(intent);
        }else if (id == R.id.action_drugs) {
            Intent intent = new Intent(getRunningInstance(), DrugsActivity.class);
            intent.putExtra("ParentClassName","ChatActivity");
            startActivity(intent);
        }else if (id == R.id.action_tip) {
            Intent intent = new Intent(getRunningInstance(), HealthTipsActivity.class);
            intent.putExtra("ParentClassName","ChatActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAuthenticationFailure(String message, Exception e) {
        System.out.println("Authentication failure: "+message);
        e.printStackTrace();
    }

    @Override
    public void onSubscriptionSucceeded(final String s) {
        System.out.println("Subscribed to: " + s);
    }

    @Override
    public void onEvent(final String channel, final String event, final String data) {
        System.out.println("Event received: " + data);

        try {
            JSONObject mssg = new JSONObject(data);
            final Chat chat = new Chat(mssg.getInt("id"), mssg.getInt("patient_id"), mssg.getInt("doctor_id"), mssg.getString("sender"), mssg.getString("message"));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.add(chat);
                    updateChatListView();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        printChats();
        System.out.println(data+" received");
//        adapter.add(new Chat());
    }
    private void printChats(){
        for(Chat chat : chats){
            System.out.println(chat);
        }
    }
    public class FetchChat extends AsyncTask<Void, Void, Void> {

        int patient_id, doctor_id, last_chat_id, limit;
        String conditions;
        FetchChat(int patient_id, int doctor_id, int last_chat_id, int limit) {
            this.patient_id = patient_id;
            this.doctor_id = doctor_id;
            this.last_chat_id = last_chat_id;
            this.limit = limit;
            conditions = "patient_id=" + patient_id + "&doctor_id=" + doctor_id + "&last_chat_id=" + last_chat_id + "&limit=" + limit;
        }

        @Override
        protected Void doInBackground(Void... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startRefreshActionRotation();
                    adapter.clear();
                }
            });

            String url = getString(R.string.server_address) + getString(R.string.list_chat_end_point) + "?" + conditions;
            String response = Service.postRequest(url, null);

            if(response != null){
                try {
                    JSONArray response_chats = new JSONArray(response);
                    if(response_chats.length() > 0){
                        for(int i=0; i < response_chats.length(); i++){
                            JSONObject set = response_chats.getJSONObject(i);
                            JSONObject json_chat = set.getJSONObject("Chat");
                            final Chat chat = new Chat(json_chat.getInt("id"), patient_id, doctor_id, json_chat.getString("sender"), json_chat.getString("message"));

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.add(chat);
                                }
                            });
//                            chats.add(chat);
//                            adapter.add(chat);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
//            ArrayAdapter adapter = new ArrayAdapter<Chat>(getRunningInstance(), R.layout.activity_chat, R.id.textView1, chats);
//            updateChatListView();
//            listView.setAdapter(adapter);
            onCancelled();

        }

        @Override
        protected void onCancelled() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    completeRefreshActionRotation();
                }
            });
        }
    }
    public class PushChat extends AsyncTask<Void, Void, Void> {

        String parameters;
        PushChat(int patient_id, int doctor_id, String message) {
            parameters = "patient_id=" + patient_id + "&doctor_id=" + doctor_id + "&message=" + message + "&sender=patient&channel="+ channelName + "&event=" + newMessageEvent;
        }

        @Override
        protected Void doInBackground(Void... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startRefreshActionRotation();
                }
            });


            String url = getString(R.string.server_address) + getString(R.string.add_chat_end_point) + "?" + parameters;
            String response = Service.postRequest(url, null);

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            onCancelled();

        }

        @Override
        protected void onCancelled() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    completeRefreshActionRotation();
                }
            });
        }
    }
    public void startRefreshActionRotation(){
        if(refreshMenuItem != null){
            Animation rotation = AnimationUtils.loadAnimation(getRunningInstance(), R.anim.rotate_refresh_icon);
            rotation.setRepeatCount(Animation.INFINITE);
            if(iv !=null){
                iv.startAnimation(rotation);
                refreshMenuItem.setActionView(iv);
                System.out.println("rotating");
            }
            refreshMenuItem.setVisible(true);
        }

    }
    public void completeRefreshActionRotation(){

        if(refreshMenuItem != null){
            refreshMenuItem.setVisible(false);
            View view = refreshMenuItem.getActionView();
            if(view !=null){
                view.clearAnimation();
            }
            refreshMenuItem.setActionView(null);

        }
    }


}

class Chat implements Serializable {
    int id, patient_id, doctor_id;
    String sender, message;
    public Chat(int id, int patient_id, int doctor_id, String sender, String message){
        this.id = id;
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
        this.sender = sender;
        this.message = message;
    }
    @Override
    public String toString() {
        return message;
    }
}
class ChatAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Chat> data = new ArrayList<Chat>();
    private static LayoutInflater inflater=null;
    public Resources res;
    Chat chat = null;
    int i=0;
    public ChatAdapter(Activity a, Resources resLocal){
        activity = a;
        res = resLocal;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        return data.size();
        return data.size() <= 0 ? 1 : data.size();
    }
    @Override
    public Object getItem(int i) {
        return data.size() <= 0 ? i : data.get(i);
    }
    @Override
    public long getItemId(int i) {
        return i;
    }
    public boolean add(Chat t){
        boolean added = this.data.add(t);
        if(added)
            this.notifyDataSetChanged();
        return added;
    }
    public void clear(){
        this.data.clear();
        this.notifyDataSetChanged();
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView message;
//        public TextView description;
//        public TextView textWide;
//        public ImageView image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate test_item.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.general, null);

            /****** View Holder Object to contain test_item.xml file elements ******/

            holder = new ViewHolder();
            holder.message = (TextView) vi.findViewById(R.id.chat_message_text_view);
//            holder.image=(ImageView)vi.findViewById(R.id.image);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        holder.message.setVisibility(View.VISIBLE);
//        holder.message.setw
        boolean useAlign = true;
        try{
            holder.message.getClass().getMethod("setTextAlignment", new Class[] {});
        }catch(Exception e){
            useAlign = false;
//            e.printStackTrace();
        }

        if(data.size()<=0)
        {
            holder.message.setText("No History Found");
            if(useAlign)
                holder.message.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            else
                holder.message.setGravity(Gravity.CENTER);
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            chat = null;
            chat = ( Chat ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.message.setText( chat.message );
            if(chat.sender.equalsIgnoreCase("patient")){
//                ColorDrawable color = new ColorDrawable();
//                color.setColor();
//                holder.message.setBackground(rs.getDrawable(R.drawable.myimage););
//                holder.message.setPadding(10, 5, 10, 5);
                if(useAlign)
                    holder.message.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                else
                    holder.message.setGravity(Gravity.LEFT);
            }else{
                if(useAlign)
                    holder.message.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                else
                    holder.message.setGravity(Gravity.RIGHT);
            }
//            holder.image.setImageResource(
//                    res.getIdentifier(
//                            "com.androidexample.customlistview:drawable/"+tempValues.getImage()
//                            ,null,null));

            /******** Set Item Click Listner for LayoutInflater for each row *******/

//            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }
}