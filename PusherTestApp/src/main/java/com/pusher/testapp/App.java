package com.pusher.testapp;

import android.app.Application;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PresenceChannel;
import com.pusher.client.channel.PresenceChannelEventListener;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.User;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by RotelandO on 6/10/2014.
 */
public class App extends Application {

    private Pusher pusher;
    private Patient patient = new Patient(0, "Default Patient", "", "");
    private String pusherAuthEndpoint = "http://lanre.bpay.ng/api/patients/auth_pusher";
    private String pusherAppKey = "68544";
    private PresenceChannel presenceChannel;
    private HashMap<String, PrivateChannel> privateChannels = new HashMap<String, PrivateChannel>();


    public Pusher getPusher(){
        if(pusher == null){
            System.out.println("new pusher created");
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("username", getPatient().username);
            params.put("password", getPatient().password);

            HttpAuthorizer authorizer = new HttpAuthorizer(getPusherAuthEndpoint());
            authorizer.setQueryStringParameters(params);
            PusherOptions options = new PusherOptions().setAuthorizer(authorizer);

            pusher = new Pusher(getPusherAppKey(), options);
            pusher.connect(new ConnectionEventListener() {
                @Override
                public void onConnectionStateChange(ConnectionStateChange change) {
                    System.out.println("State changed to " + change.getCurrentState() +
                            " from " + change.getPreviousState());
                }

                @Override
                public void onError(String message, String code, Exception e) {
                    System.out.println("There was a problem connecting!");
                }
            }, ConnectionState.ALL);
            presenceChannel = pusher.subscribePresence("presence-user", new PresenceChannelEventListener() {
                @Override
                public void userUnsubscribed(String s, User user) {
                    System.out.println("removed subscription" + s);
                    System.out.println("user" + user);
                }
                @Override
                public void userSubscribed(String s, User user) {
                    System.out.println("new subscription" + s);
                    System.out.println("user" + user);
                }
                @Override
                public void onAuthenticationFailure(String s, Exception e) {
                    e.printStackTrace();
                    System.out.println("subscription error" + s);
                }
                @Override
                public void onUsersInformationReceived(String s, Set<User> u) {

                }

                @Override
                public void onSubscriptionSucceeded(String a) {
                    System.out.println("event a" + a);
                }
                @Override
                public void onEvent(String a, String b, String c) {
                    System.out.println("event a" + a);
                    System.out.println("event b" + b);
                    System.out.println("event c" + c);
                }
            }, "");
            System.out.println("c "+presenceChannel);
            System.out.println("presence here "+presenceChannel);
        }
        return pusher;
    }
    public PresenceChannel getPresenceChannel(){
        if(presenceChannel == null){
            getPusher();
        }
        return presenceChannel;
    }
//    public PrivateChannel getPrivateChannel(String name){
//        if(!privateChannels.containsKey(name)){
//            return null;
//        }
//        return privateChannels.get(name);
//    }
//    public void addPrivateChannel(String name, PrivateChannelEventListener listener, String... events){
//        if(!privateChannels.containsKey(name)){
//            privateChannels.put(name, getPusher().subscribePrivate(name, listener, events));
//        }
//    }
//    public void getPrivateChannel(String name, PrivateChannelEventListener listener, String... events){
//        return getPusher().subscribePrivate(name, listener, events);
//    }
    public Patient getPatient(){
        return patient;
    }
    public void setPatient(Patient pat){
        patient = pat;
    }
    public String getPusherAuthEndpoint(){
        return pusherAuthEndpoint;
    }
    public void setPusherAuthEndPoint(String n){
        pusherAuthEndpoint = n;
    }
    public String getPusherAppKey(){
        return pusherAppKey;
    }
    public void setPusherAppKey(String n){
        pusherAppKey = n;
    }

}
