package com.pusher.testapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PresenceChannel;
import com.pusher.client.channel.impl.PresenceChannelImpl;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * A login screen that offers login via email/password.

 */
public class LoginActivity extends Activity{

    public static int USER_ID = 0;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mCardNumberView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mCardNumberView = (EditText) findViewById(R.id.card_number);

        mPasswordView = (EditText) findViewById(R.id.password);
        mCardNumberView.setText("Lanrekas91@gmail.com");
        mPasswordView.setText("Lanrekas");

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Button _temp = (Button) findViewById(R.id.go_to_register_button);
        _temp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getRunningInstance(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_faq) {
            Intent intent = new Intent(getRunningInstance(), FAQActivity.class);
            intent.putExtra("ParentClassName","LoginActivity");
            startActivity(intent);
        }else if (id == R.id.action_tip) {
            Intent intent = new Intent(getRunningInstance(), HealthTipsActivity.class);
            intent.putExtra("ParentClassName","LoginActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mCardNumberView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String cardNumber = mCardNumberView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(cardNumber)) {
            mCardNumberView.setError(getString(R.string.error_field_required));
            focusView = mCardNumberView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(cardNumber, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private LoginActivity getRunningInstance(){
        return this;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private String mUsername;
        private String mPassword;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected String doInBackground(Void... params) {
            ArrayList<NameValuePair> para = new ArrayList<NameValuePair>();
            para.add(new BasicNameValuePair("username", mUsername));
            para.add(new BasicNameValuePair("password", mPassword));
            String url = getString(R.string.server_address) + getString(R.string.login_end_point);

            String str = Service.postRequest(url, para);
            return str;
        }

        @Override
        protected void onPostExecute(final String response) {

            if(response != null){
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getString("status").equalsIgnoreCase("ok")){
                        Intent intent = new Intent(getRunningInstance(), DepartmentsActivity.class);
                        System.out.println("Creating patient");
                        JSONObject tmp = json.getJSONObject("patient").getJSONObject("Patient");                        System.out.println("I have "+tmp.getInt("id")+" and "+tmp.getString("name"));
                        Patient patient = new Patient(tmp.getInt("id"), tmp.getString("name"), mUsername, mPassword);

                        App app = (App) getApplicationContext();
                        app.setPatient(patient);
                        app.setPusherAppKey(getString(R.string.pusher_app_key));
                        app.setPusherAuthEndPoint(getString(R.string.server_address) + getString(R.string.pusher_app_auth_end_point));

                        app.getPusher();

                        startActivity(intent);
                        finish();
                    }else{
                        String message = json.getString("message");
                        mAuthTask = null;
                        showProgress(false);
                        if(json.getString("target").equalsIgnoreCase("username")){
                            mCardNumberView.setError(json.getString("message"));
                            mCardNumberView.requestFocus();
                        }else{
                            mPasswordView.setError(json.getString("message"));
                            mPasswordView.requestFocus();
                        }
                    }
                }catch (Exception e){

                }

            }else{
                mAuthTask = null;
                showProgress(false);
                mCardNumberView.setError(getString(R.string.error_connecting));
                mCardNumberView.requestFocus();
            }


        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
    @Override
     public void onDestroy() {
        super.onDestroy();  // Always call the superclass

        // Stop method tracing that the activity started during onCreate()
        android.os.Debug.stopMethodTracing();
    }
}
class Patient implements Serializable{
    int id;
    String name, username, password;
    public Patient(int id, String name, String username, String password){
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
    }
}