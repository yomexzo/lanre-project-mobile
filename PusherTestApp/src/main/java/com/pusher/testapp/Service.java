package com.pusher.testapp;

import android.app.Activity;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PresenceChannel;
import com.pusher.client.channel.User;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by RotelandO on 5/17/2014.
 */
public class Service extends Activity{
    public static String postRequest(String url, ArrayList<NameValuePair> parameter){
        try {
            if(parameter != null) {
                url += "?";
                for (int i = 0; i < parameter.size(); i++) {
                   url+= parameter.get(i).getName()+"="+ URLEncoder.encode(parameter.get(i).getValue(), HTTP.UTF_8) +"&";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        String result = null;
        InputStream inputStream = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            System.out.println(url);
            httppost.setHeader("Content-type", "application/json");
            if(parameter != null) {
                httppost.setEntity(new UrlEncodedFormEntity(parameter));
                System.out.println(parameter.size()+" parameters sent");
            }

            HttpEntity entity = httpClient.execute(httppost).getEntity();

            inputStream = entity.getContent();
            // json is UTF-8 by default
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            result = sb.toString();
        } catch (Exception e) {
            // Oops
            e.printStackTrace();
        }
        finally {
            try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
        }
        System.out.println("response: "+result);
        return result;
    }
}
