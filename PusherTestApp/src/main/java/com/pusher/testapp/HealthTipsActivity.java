package com.pusher.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class HealthTipsActivity extends ActionBarActivity {
    ListView listView;
    View progressView;
    HealthTipsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_tips);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        listView = (ListView) findViewById(R.id.tipList);
        progressView = findViewById(R.id.tipProgress);

        adapter = new HealthTipsAdapter(getRunningInstance(), getResources());
        listView.setAdapter(adapter);

        populateTips();
    }

    public void populateTips(){
        adapter.add(new HealthTip("Go Fish", "If you suffer from dry eyes, up your seafood intake. Salmon, sardines, and mackerel contain omega-3 fatty acids, which the body uses to produce tears, among other things. Research suggests that people who consume higher amounts of these fats are less likely to have dry eyes."));
        adapter.add(new HealthTip("Eat Bananas", "People whose diets are rich in potassium may be less prone to high blood pressure. Besides reducing sodium and taking other heart-healthy steps, eat potassium-packed picks such as bananas, cantaloupe, and oranges."));
        adapter.add(new HealthTip("Get a Massage", "Certain trigger points -- spots of tension in musculoskeletal tissue -- can cause back pain. Ask a massage therapist or other bodyworker who specializes in myofascial release or neuromuscular therapy to focus on these points during a massage."));
        adapter.add(new HealthTip("Go for Garlic", "Adding raw or lightly cooked garlic and onions to your meals may help keep you healthy this winter. Both foods appear to possess antiviral and antibacterial properties and are believed to boost immunity."));
        adapter.add(new HealthTip("Eat Avocados", "For dry skin, incorporate more avocados into your diet. They're rich in monounsaturated fat and vitamin E, both of which promote healthy skin. Try them on salads and sandwiches, and even in smoothies."));
    }

    public HealthTipsActivity getRunningInstance(){
        return this;
    }

    @Override
    public Intent getSupportParentActivityIntent (){

        Intent parentIntent= getIntent();
        String className = parentIntent.getStringExtra("ParentClassName"); //getting the parent class name

        Intent newIntent=null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(HealthTipsActivity.this,Class.forName("com.pusher.testapp."+className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.health_tips, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

}

class HealthTip{
    public String title, description;
    public HealthTip(String title, String description){
        this.title = title;
        this.description = description;
    }
}
class HealthTipsAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<HealthTip> data = new ArrayList<HealthTip>();
    private static LayoutInflater inflater=null;
    public Resources res;
    HealthTip tip = null;
    int i=0;
    public HealthTipsAdapter(Activity a, Resources resLocal){
        activity = a;
        res = resLocal;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        return data.size();
        return data.size() <= 0 ? 1 : data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.size() <= 0 ? i : data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public boolean add(HealthTip t){
        boolean added = this.data.add(t);
        if(added)
            this.notifyDataSetChanged();
        return added;
    }
    public void clear(){
        this.data.clear();
        this.notifyDataSetChanged();
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView name;
        public TextView description;
//        public TextView textWide;
//        public ImageView image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate test_item.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.test_item, null);

            /****** View Holder Object to contain test_item.xml file elements ******/

            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.testItemName);
            holder.description = (TextView)vi.findViewById(R.id.testItemDescription);
//            holder.image=(ImageView)vi.findViewById(R.id.image);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.name.setText("No Data");
            holder.description.setText("");
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tip = null;
            tip = ( HealthTip ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.name.setText( tip.title );
            holder.name.setTypeface(Typeface.create("bold", Typeface.BOLD));
            holder.description.setTypeface(Typeface.create("normal", Typeface.NORMAL));
            int l = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, res.getDisplayMetrics());
            int t_r = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, res.getDisplayMetrics());
            int b = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, res.getDisplayMetrics());
            holder.description.setPadding(l, t_r, t_r, b);
            holder.description.setText(tip.description);
        }
        return vi;
    }
}
