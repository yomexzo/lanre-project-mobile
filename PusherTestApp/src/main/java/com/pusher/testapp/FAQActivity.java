package com.pusher.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class FAQActivity extends ActionBarActivity {

    ListView listView;
    View progressView;
    FAQAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        listView = (ListView) findViewById(R.id.faqList);
        progressView = findViewById(R.id.faqProgress);

        adapter = new FAQAdapter(getRunningInstance(), getResources());
        listView.setAdapter(adapter);

        populateFAQs();
    }
    public void populateFAQs(){
        adapter.add(new FAQ("Application suddenly displaying unfortunately stopped.", "Solution: reopen the application and enter email address and password"));
        adapter.add(new FAQ("Application not displaying the list of departments or doctors.", "Solution: check your network signal and ensure you have a strong signal and reload the page."));
        adapter.add(new FAQ("Application not displaying the chat history or typed message.", "Solution: ensure you have a strong signal and reload the page."));
        adapter.add(new FAQ("Registration", "The app provides a registration form to provide your information which is validated. Once registration is complete, you can register and enjoy the Crescent University MHealth Application"));
        adapter.add(new FAQ("Departments", "Once you're logged into the Crescent University MHealth Application, you'll be taken to the Departments view where you see all the departments available in the hospital"));
        adapter.add(new FAQ("Doctors", "Accessing a list of doctors is per department. Select a department to view a list of the doctors in it."));
        adapter.add(new FAQ("Chat", "Once you select a particular doctor, you'll be able to send chat back and forth with the doctor. Your sent messages are to the left and your received messages are to the right."));
        adapter.add(new FAQ("Prescriptions", "This view allows you to see a history of all drugs that have been recommended by a doctor at one point or the other at any point in time."));
        adapter.add(new FAQ("Tests", "This view allows you to see a history of all tests that have been recommended by a doctor at one point or the other at any point in time."));
    }

    @Override
    public Intent getSupportParentActivityIntent (){

        Intent parentIntent= getIntent();
        String className = parentIntent.getStringExtra("ParentClassName"); //getting the parent class name

        Intent newIntent=null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(FAQActivity.this,Class.forName("com.pusher.testapp."+className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    @Override
    public void onCreateSupportNavigateUpTaskStack (TaskStackBuilder builder){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        if(((App)getApplicationContext()).getPatient() == null)
            getMenuInflater().inflate(R.menu.faq, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    public FAQActivity getRunningInstance(){
        return this;
    }
}

class FAQ{
    public String title, description;
    public FAQ(String title, String description){
        this.title = title;
        this.description = description;
    }
}
class FAQAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<FAQ> data = new ArrayList<FAQ>();
    private static LayoutInflater inflater=null;
    public Resources res;
    FAQ faq = null;
    int i=0;
    public FAQAdapter(Activity a, Resources resLocal){
        activity = a;
        res = resLocal;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        return data.size();
        return data.size() <= 0 ? 1 : data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.size() <= 0 ? i : data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public boolean add(FAQ t){
        boolean added = this.data.add(t);
        if(added)
            this.notifyDataSetChanged();
        return added;
    }
    public void clear(){
        this.data.clear();
        this.notifyDataSetChanged();
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView name;
        public TextView description;
//        public TextView textWide;
//        public ImageView image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate test_item.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.test_item, null);

            /****** View Holder Object to contain test_item.xml file elements ******/

            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.testItemName);
            holder.description = (TextView)vi.findViewById(R.id.testItemDescription);
//            holder.image=(ImageView)vi.findViewById(R.id.image);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.name.setText("No Data");
            holder.description.setText("");
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            faq = null;
            faq = ( FAQ ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.name.setText( faq.title );
            Typeface tf = holder.description.getTypeface();
            holder.description.setTypeface(Typeface.create("bold", Typeface.BOLD));
            int l = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, res.getDisplayMetrics());
            int t_r = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, res.getDisplayMetrics());
            int b = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, res.getDisplayMetrics());
            holder.description.setPadding(l, t_r, t_r, b);
            holder.description.setText(faq.description);
//            holder.image.setImageResource(
//                    res.getIdentifier(
//                            "com.androidexample.customlistview:drawable/"+tempValues.getImage()
//                            ,null,null));

            /******** Set Item Click Listner for LayoutInflater for each row *******/

//            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }
}
