package com.pusher.testapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


public class DoctorsActivity extends ActionBarActivity {

    ListView listView;
    ImageView iv;
    MenuItem refreshMenuItem;
    Department department;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            department = (Department) getIntent().getSerializableExtra("department");
            System.out.println("New instance department: "+department);
        } else {
            department= (Department)savedInstanceState.getSerializable("department");
            System.out.println("Saved instance department: "+department);
        }
        if(department == null){
            department = new Department(0, "Go back!");
        }
        setTitle(department.doctorTitle);
        setContentView(R.layout.activity_doctors);



        listView = (ListView) findViewById(R.id.list_doctors_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getRunningInstance(), ChatActivity.class);
                Doctor doctor = (Doctor) adapterView.getAdapter().getItem(position);
                intent.putExtra("doctor", doctor);
                startActivity(intent);
            }
        });


        /* Time to cache inflated ImageView for rotation */
        LayoutInflater inflater = (LayoutInflater) getRunningInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        iv = (ImageView) inflater.inflate(R.layout.refresh_action_view, null);

        populateDoctors();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.doctors, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        refreshMenuItem = menu.findItem(R.id.action_refresh_doctors);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh_doctors) {
            populateDoctors();
        }else if (id == R.id.action_faq) {
            Intent intent = new Intent(getRunningInstance(), FAQActivity.class);
            intent.putExtra("ParentClassName","DoctorsActivity");
            startActivity(intent);
        }else if (id == R.id.action_tests) {
            Intent intent = new Intent(getRunningInstance(), TestsActivity.class);
            intent.putExtra("ParentClassName","DoctorsActivity");
            startActivity(intent);
        }else if (id == R.id.action_drugs) {
            Intent intent = new Intent(getRunningInstance(), DrugsActivity.class);
            intent.putExtra("ParentClassName","DoctorsActivity ");
            startActivity(intent);
        }else if (id == R.id.action_tip) {
            Intent intent = new Intent(getRunningInstance(), HealthTipsActivity.class);
            intent.putExtra("ParentClassName","DoctorsActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void populateDoctors(){
        ArrayList<NameValuePair> conditions = null;
        if(department.id != 0){
            conditions = new ArrayList<NameValuePair>();
            conditions.add(new BasicNameValuePair("department_id", ""+department.id));
        }
        FetchDoctor fetch = new FetchDoctor(conditions);
        fetch.execute((Void) null);
    }

    public DoctorsActivity getRunningInstance(){
        return this;
    }

    public void startRefreshActionRotation(){

        if(refreshMenuItem != null){
            Animation rotation = AnimationUtils.loadAnimation(getRunningInstance(), R.anim.rotate_refresh_icon);
            rotation.setRepeatCount(Animation.INFINITE);
            if(iv !=null){
                iv.startAnimation(rotation);
                refreshMenuItem.setActionView(iv);
                System.out.println("rotating");
            }
        }
    }

    public void completeRefreshActionRotation(){
        if(refreshMenuItem != null){
            View view = refreshMenuItem.getActionView();
            if(view !=null){
                view.clearAnimation();
            }
            refreshMenuItem.setActionView(null);

        }
    }

    public class FetchDoctor extends AsyncTask<Void, Void, ArrayAdapter> {

        String conditions = "";
        FetchDoctor(ArrayList<NameValuePair> conditions) {
            if(conditions != null){
                for(NameValuePair condition : conditions){
                    this.conditions += condition.getName()+"="+condition.getValue()+"&";
                }
            }
        }

        @Override
        protected ArrayAdapter doInBackground(Void... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startRefreshActionRotation();
                }
            });

            String url = getString(R.string.server_address) + getString(R.string.list_doctors_end_point) + "?" + conditions;
            String response = Service.postRequest(url, null);

            //for debugging purpose
            System.out.println(response);

            ArrayList<Doctor> depts = new ArrayList<Doctor>();

            if(response != null){
                try {
                    JSONArray response_depts = new JSONArray(response);
                    if(response_depts.length() > 0){
                        for(int i=0; i < response_depts.length(); i++){
                            JSONObject dept = response_depts.getJSONObject(i);
                            depts.add(new Doctor(dept.getJSONObject("Doctor").getInt("id"), dept.getJSONObject("Doctor").getString("name")));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<Doctor>(getRunningInstance(), R.layout.activity_departments, R.id.textView1, depts);
            return adapter;
        }

        @Override
        protected void onPostExecute(final ArrayAdapter adapter) {
            listView.setAdapter(adapter);

            onCancelled();

        }

        @Override
        protected void onCancelled() {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    completeRefreshActionRotation();
                }
            });
        }
    }


}

class Doctor implements Serializable{
    String name, chatTitle;
    int id;
    public Doctor(int id, String name){
        this.id = id;
        this.name = name;
        this.chatTitle = name;
    }
    @Override
    public String toString() {
        return name;
    }
}