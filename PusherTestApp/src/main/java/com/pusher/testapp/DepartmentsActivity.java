package com.pusher.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


public class DepartmentsActivity extends ActionBarActivity {
    public static String DEPARTMENT_POSITION = "department_position";
    ListView listView;
    ImageView iv;
    MenuItem refreshMenuItem;
    Patient patient;
    ArrayAdapter listAdapter;
    ArrayList<Department> deptsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("About to set content view");
        setContentView(R.layout.activity_departments);
        System.out.println("about to fetch list view");
        listView = (ListView) findViewById(R.id.list_departments_view);
        System.out.println("about to set onitemclicklistener");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getRunningInstance(), DoctorsActivity.class);
                Department dept = (Department) adapterView.getAdapter().getItem(position);
                intent.putExtra("department", dept);
                startActivity(intent);
            }
        });
        System.out.println("about to inflate refresh action view");
        /* Time to cache inflated ImageView for rotation */
        LayoutInflater inflater = (LayoutInflater) getRunningInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        iv = (ImageView) inflater.inflate(R.layout.refresh_action_view, null);

        deptsList = new ArrayList<Department>();
        listAdapter = new ArrayAdapter<Department>(getRunningInstance(), R.layout.activity_departments, R.id.textView1, deptsList);
        listView.setAdapter(listAdapter);

    }

    public void populateDepartments(){
        FetchDepartment fetch = new FetchDepartment(null);
        fetch.execute((Void) null);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.departments, menu);
        refreshMenuItem = menu.findItem(R.id.action_refresh_departments);
        populateDepartments();
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh_departments) {
            populateDepartments();
        }else if (id == R.id.action_faq) {
            Intent intent = new Intent(getRunningInstance(), FAQActivity.class);
            intent.putExtra("ParentClassName","DepartmentsActivity");
            startActivity(intent);
        }else if (id == R.id.action_tests) {
            Intent intent = new Intent(getRunningInstance(), TestsActivity.class);
            intent.putExtra("ParentClassName","DepartmentsActivity");
            startActivity(intent);
        }else if (id == R.id.action_drugs) {
            Intent intent = new Intent(getRunningInstance(), DrugsActivity.class);
            intent.putExtra("ParentClassName","DepartmentsActivity");
            startActivity(intent);
        }else if (id == R.id.action_tip) {
            Intent intent = new Intent(getRunningInstance(), HealthTipsActivity.class);
            intent.putExtra("ParentClassName","DepartmentsActivity");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public DepartmentsActivity getRunningInstance(){
        return this;
    }

    public class FetchDepartment extends AsyncTask<Void, Void, ArrayAdapter> {

        String conditions = "";
        FetchDepartment(ArrayList<NameValuePair> conditions) {
            if(conditions != null){
                for(NameValuePair condition : conditions){
                    this.conditions += condition.getName()+"="+condition.getValue()+"&";
                }
            }
        }

        @Override
        protected ArrayAdapter doInBackground(Void... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startRefreshActionRotation();
                }
            });


            String url = getString(R.string.server_address) + getString(R.string.list_departments_end_point) + "?" + conditions;
            String response = Service.postRequest(url, null);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    deptsList.clear();
                    listAdapter.notifyDataSetChanged();
                }
            });
//            ArrayList<Department> depts = new ArrayList<Department>();

            if(response != null){
                try {

                    JSONArray response_depts = new JSONArray(response);
                    if(response_depts.length() > 0){
                        for(int i=0; i < response_depts.length(); i++){
                            JSONObject dept = response_depts.getJSONObject(i);
                            deptsList.add(new Department(dept.getJSONObject("Department").getInt("id"), dept.getJSONObject("Department").getString("name")));
//                            listAdapter.notifyDataSetChanged();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    listAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

//            ArrayAdapter adapter = new ArrayAdapter<Department>(getRunningInstance(), R.layout.activity_departments, R.id.textView1, deptsList);
            return null;
        }

        @Override
        protected void onPostExecute(final ArrayAdapter adapter) {

//            listView.setAdapter(adapter);
            onCancelled();

        }

        @Override
        protected void onCancelled() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    completeRefreshActionRotation();
                }
            });
        }
    }

    public void startRefreshActionRotation(){

        if(refreshMenuItem != null){
            Animation rotation = AnimationUtils.loadAnimation(getRunningInstance(), R.anim.rotate_refresh_icon);
            rotation.setRepeatCount(Animation.INFINITE);
            if(iv !=null){
                iv.startAnimation(rotation);
                refreshMenuItem.setActionView(iv);
                System.out.println("rotating");
            }
        }
    }
    public void completeRefreshActionRotation(){
        if(refreshMenuItem != null){
            View view = refreshMenuItem.getActionView();
            if(view !=null){
                view.clearAnimation();
            }
            refreshMenuItem.setActionView(null);

        }
    }
}

class Department implements Serializable{
    String name, doctorTitle;
    int id;
    public Department(int id, String name){
        this.id = id;
        this.name = name;
        this.doctorTitle = name + " department";
    }
    @Override
    public String toString() {
        return name;
    }
}