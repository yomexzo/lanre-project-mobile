package com.pusher.testapp;

import com.pusher.client.Authorizer;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.impl.PresenceChannelImpl;
import com.pusher.client.connection.impl.InternalConnection;
import com.pusher.client.util.Factory;

import java.io.Serializable;

public class PusherWrapper implements Serializable {
    Pusher pusher;
    public PusherWrapper() {

    }
    public PusherWrapper(String apiKey) {
        pusher = new Pusher(apiKey);
    }

    public PusherWrapper(String apiKey, PusherOptions pusherOptions) {
        pusher = new Pusher(apiKey, pusherOptions);
    }
}
class MyPresenceChannelImpl extends PresenceChannelImpl implements Serializable {

    public MyPresenceChannelImpl(InternalConnection connection, String channelName,
                               Authorizer authorizer, Factory factory) {
        super(connection, channelName, authorizer, factory);
    }
}
